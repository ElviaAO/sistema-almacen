// import express
import express from "express";
  

import { Login, showProducto, consulProveedores, consulPersona, consulReporte} from "../controllers/almance.js";
  
// init express router
const router = express.Router();
  

router.post('/login', Login);
  
router.get('/producto', showProducto);
  
router.get('/proveedores', consulProveedores);

router.get('/persona', consulPersona);

router.get('/reporte', consulReporte);
  
// Update Product
//router.put('/products/:id', updateProduct);
  
// Delete Product
//router.delete('/products/:id', deleteProduct);
  
// export default router
export default router;