
// Import function from Product Model
import {  Login, showProducto, consulProveedores, consulPersona, consulReporte, getProducto, insertProduct } from "../models/almanceModel.js";
  
// Get All Products
export const Login= (result) => {
    postLogin((error, results) => {
        if (error){
            res.send(error);
        }else{
            res.json(results);
        }
    });
}
  
export const producto = (req, res) => {
    getProducto(req.params.id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}


export const proveedores = (req, res) => {
    getProveedores(req.params.id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}
  
// Create New Product
export const consulPersona = (req, res) => {
    const data = req.body;
    getPersona(data, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

export const reporte = (req, res) => {
    const data = req.body;
    getReporte(data, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}
  
// // Update Product
// export const updateProduct = (req, res) => {
//     const data  = req.body;
//     const id    = req.params.id;
//     updateProductById(data, id, (err, results) => {
//         if (err){
//             res.send(err);
//         }else{
//             res.json(results);
//         }
//     });
// }
  
// // Delete Product
// export const deleteProduct = (req, res) => {
//     const id = req.params.id;
//     deleteProductById(id, (err, results) => {
//         if (err){
//             res.send(err);
//         }else{
//             res.json(results);
//         }
//     });
// }
