// import connection
import db from "../config/data.js";
  

export const getLogin = (result) => {
    if (username && password) {
        db.query("SELECT * FROM login WHERE usuario = ? AND clave = ?", [username, password], (err, results) => {             
        if (error) throw error;
      
        if (results.length > 0) {
        
            request.session.loggedin = true;
            request.session.username = username;
        
            response.redirect('/home');
      } else {
        response.send('Usuario y/o Contraseña Incorrecta');
      }     
      response.end();
        }); 
    }  
}
  

export const getProducto = (id, result) => {
    db.query("SELECT * FROM productos", (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results[0]);
        }
    });   
}

export const getProveedores = (id, result) => {
    db.query("SELECT * FROM proveedores ", (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results[0]);
        }
    });   
}

export const consulPersona = (id, result) => {
    db.query("SELECT * FROM persona", (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results[0]);
        }
    });   
}

export const reporte = (id, result) => {
    db.query("SELECT * FROM reportes ", (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results[0]);
        }
    });   
}


// // Insert Product to Database
// export const insertProduct = (data, result) => {
//     db.query("INSERT INTO product SET ?", [data], (err, results) => {             
//         if(err) {
//             console.log(err);
//             result(err, null);
//         } else {
//             result(null, results);
//         }
//     });   
// }
  
// // Update Product to Database
// export const updateProductById = (data, id, result) => {
//     db.query("UPDATE product SET product_name = ?, product_price = ? WHERE product_id = ?", [data.product_name, data.product_price, id], (err, results) => {             
//         if(err) {
//             console.log(err);
//             result(err, null);
//         } else {
//             result(null, results);
//         }
//     });   
// }
  
// // Delete Product to Database
// export const deleteProductById = (id, result) => {
//     db.query("DELETE FROM product WHERE product_id = ?", [id], (err, results) => {             
//         if(err) {
//             console.log(err);
//             result(err, null);
//         } else {
//             result(null, results);
//         }
//     });   
// }