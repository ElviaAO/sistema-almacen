-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-12-2022 a las 10:31:54
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `almacen`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `idadministrador` int(11) NOT NULL,
  `tipo_usuario_idtipo_usuario` int(11) NOT NULL,
  `reportes_idreportes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `chequeo`
--

CREATE TABLE `chequeo` (
  `idchequeo` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `producto_idproducto` int(11) NOT NULL,
  `supervisor_idsupervisor` int(11) NOT NULL,
  `estatus_idestatus` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clasificacion_producto`
--

CREATE TABLE `clasificacion_producto` (
  `idclasificacion_ropa` int(11) NOT NULL,
  `descripcion` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `idclientes` int(11) NOT NULL,
  `tipo_usuario_idtipo_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes_has_producto`
--

CREATE TABLE `clientes_has_producto` (
  `clientes_idclientes` int(11) NOT NULL,
  `producto_idproducto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `codigo_area`
--

CREATE TABLE `codigo_area` (
  `idcodigo_area` int(11) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `codigo_area`
--

INSERT INTO `codigo_area` (`idcodigo_area`, `descripcion`) VALUES
(1, '0212'),
(2, '0412'),
(3, '0414'),
(4, '0424'),
(5, '0416'),
(6, '0426');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_civil`
--

CREATE TABLE `estado_civil` (
  `idestado_civil` int(11) NOT NULL,
  `descripcion` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `estado_civil`
--

INSERT INTO `estado_civil` (`idestado_civil`, `descripcion`) VALUES
(1, 'Soltero'),
(2, 'Casado'),
(3, 'Viudo'),
(4, 'Divorciado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estatus`
--

CREATE TABLE `estatus` (
  `idestatus` int(11) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `genero`
--

CREATE TABLE `genero` (
  `idgenero` int(11) NOT NULL,
  `descripcion` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `genero`
--

INSERT INTO `genero` (`idgenero`, `descripcion`) VALUES
(1, 'Masculino'),
(2, 'Femenino');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localidad`
--

CREATE TABLE `localidad` (
  `idlocalidad` int(11) NOT NULL,
  `descripcion` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `localidad`
--

INSERT INTO `localidad` (`idlocalidad`, `descripcion`) VALUES
(1, 'Caracas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login`
--

CREATE TABLE `login` (
  `usuario` varchar(45) NOT NULL,
  `clave` varchar(45) NOT NULL,
  `tipo_usuario_idtipo_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `login`
--

INSERT INTO `login` (`usuario`, `clave`, `tipo_usuario_idtipo_usuario`) VALUES
('elvia', '1234', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nacionalidad`
--

CREATE TABLE `nacionalidad` (
  `idnacionalidad` int(11) NOT NULL,
  `descripcion` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nacionalidad`
--

INSERT INTO `nacionalidad` (`idnacionalidad`, `descripcion`) VALUES
(1, 'Venezolano'),
(2, 'Extranjero');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `idpersona` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `fechanac` date NOT NULL,
  `genero_idgenero` int(11) NOT NULL,
  `nacionalidad_idnacionalidad` int(11) NOT NULL,
  `nro_cedula` varchar(20) NOT NULL,
  `estado_civil_idestado_civil` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`idpersona`, `nombre`, `apellido`, `fechanac`, `genero_idgenero`, `nacionalidad_idnacionalidad`, `nro_cedula`, `estado_civil_idestado_civil`) VALUES
(1, 'Elvia', 'Orozco', '1993-10-16', 2, 1, '23925965', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona_has_localidad`
--

CREATE TABLE `persona_has_localidad` (
  `persona_idpersona` int(11) NOT NULL,
  `localidad_idlocalidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `persona_has_localidad`
--

INSERT INTO `persona_has_localidad` (`persona_idpersona`, `localidad_idlocalidad`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona_has_telefonos`
--

CREATE TABLE `persona_has_telefonos` (
  `persona_idpersona` int(11) NOT NULL,
  `telefonos_idtelefonos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `persona_has_telefonos`
--

INSERT INTO `persona_has_telefonos` (`persona_idpersona`, `telefonos_idtelefonos`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `idproducto` int(11) NOT NULL,
  `color` varchar(45) NOT NULL,
  `material` varchar(45) NOT NULL,
  `tamanos_idtamanos` int(11) NOT NULL,
  `tipo_producto_idtipo_producto` int(11) NOT NULL,
  `marca` varchar(45) NOT NULL,
  `departamento` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `idproveedores` int(11) NOT NULL,
  `tipo_usuario_idtipo_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores_has_producto`
--

CREATE TABLE `proveedores_has_producto` (
  `proveedores_idproveedores` int(11) NOT NULL,
  `producto_idproducto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reportes`
--

CREATE TABLE `reportes` (
  `idreportes` int(11) NOT NULL,
  `historial` varchar(45) NOT NULL,
  `chequeo_idchequeo` int(11) NOT NULL,
  `producto_idproducto` int(11) NOT NULL,
  `supervisor_idsupervisor` int(11) NOT NULL,
  `proveedores_idproveedores` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `supervisor`
--

CREATE TABLE `supervisor` (
  `idsupervisor` int(11) NOT NULL,
  `tipo_usuario_idtipo_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tamanos`
--

CREATE TABLE `tamanos` (
  `idtamanos` int(11) NOT NULL,
  `descripcion` varchar(45) NOT NULL,
  `tipo_producto_idtipo_producto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `telefonos`
--

CREATE TABLE `telefonos` (
  `idtelefonos` int(11) NOT NULL,
  `nro` varchar(20) NOT NULL,
  `codigo_area_idcodigo_area` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `telefonos`
--

INSERT INTO `telefonos` (`idtelefonos`, `nro`, `codigo_area_idcodigo_area`) VALUES
(1, '2799194', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_producto`
--

CREATE TABLE `tipo_producto` (
  `idtipo_producto` int(11) NOT NULL,
  `decripcion` varchar(45) NOT NULL,
  `clasificacion_producto_idclasificacion_ropa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_usuario`
--

CREATE TABLE `tipo_usuario` (
  `idtipo_usuario` int(11) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_usuario`
--

INSERT INTO `tipo_usuario` (`idtipo_usuario`, `descripcion`) VALUES
(1, 'Administrador');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`idadministrador`),
  ADD KEY `fk_administrador_tipo_usuario1_idx` (`tipo_usuario_idtipo_usuario`),
  ADD KEY `fk_administrador_reportes1_idx` (`reportes_idreportes`);

--
-- Indices de la tabla `chequeo`
--
ALTER TABLE `chequeo`
  ADD PRIMARY KEY (`idchequeo`),
  ADD KEY `fk_chequeo_producto1_idx` (`producto_idproducto`),
  ADD KEY `fk_chequeo_supervisor1_idx` (`supervisor_idsupervisor`),
  ADD KEY `fk_chequeo_estatus1_idx` (`estatus_idestatus`);

--
-- Indices de la tabla `clasificacion_producto`
--
ALTER TABLE `clasificacion_producto`
  ADD PRIMARY KEY (`idclasificacion_ropa`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`idclientes`),
  ADD KEY `fk_clientes_tipo_usuario1_idx` (`tipo_usuario_idtipo_usuario`);

--
-- Indices de la tabla `clientes_has_producto`
--
ALTER TABLE `clientes_has_producto`
  ADD KEY `fk_clientes_has_producto_clientes1_idx` (`clientes_idclientes`),
  ADD KEY `fk_clientes_has_producto_producto1_idx` (`producto_idproducto`);

--
-- Indices de la tabla `codigo_area`
--
ALTER TABLE `codigo_area`
  ADD PRIMARY KEY (`idcodigo_area`);

--
-- Indices de la tabla `estado_civil`
--
ALTER TABLE `estado_civil`
  ADD PRIMARY KEY (`idestado_civil`);

--
-- Indices de la tabla `estatus`
--
ALTER TABLE `estatus`
  ADD PRIMARY KEY (`idestatus`);

--
-- Indices de la tabla `genero`
--
ALTER TABLE `genero`
  ADD PRIMARY KEY (`idgenero`);

--
-- Indices de la tabla `localidad`
--
ALTER TABLE `localidad`
  ADD PRIMARY KEY (`idlocalidad`);

--
-- Indices de la tabla `login`
--
ALTER TABLE `login`
  ADD KEY `fk_login_administrador1_idx` (`tipo_usuario_idtipo_usuario`);

--
-- Indices de la tabla `nacionalidad`
--
ALTER TABLE `nacionalidad`
  ADD PRIMARY KEY (`idnacionalidad`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`idpersona`),
  ADD KEY `fk_persona_nacionalidad1_idx` (`nacionalidad_idnacionalidad`),
  ADD KEY `fk_persona_estado_civil1_idx` (`estado_civil_idestado_civil`),
  ADD KEY `fk_persona_genero1_idx` (`genero_idgenero`);

--
-- Indices de la tabla `persona_has_localidad`
--
ALTER TABLE `persona_has_localidad`
  ADD KEY `fk_persona_has_localidad_persona1_idx` (`persona_idpersona`),
  ADD KEY `fk_persona_has_localidad_localidad1_idx` (`localidad_idlocalidad`);

--
-- Indices de la tabla `persona_has_telefonos`
--
ALTER TABLE `persona_has_telefonos`
  ADD KEY `fk_persona_has_telefonos_persona1_idx` (`persona_idpersona`),
  ADD KEY `fk_persona_has_telefonos_telefonos1_idx` (`telefonos_idtelefonos`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`idproducto`),
  ADD KEY `fk_producto_tamanos1_idx` (`tamanos_idtamanos`),
  ADD KEY `fk_producto_tipo_producto1_idx` (`tipo_producto_idtipo_producto`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`idproveedores`),
  ADD KEY `fk_proveedores_tipo_usuario1_idx` (`tipo_usuario_idtipo_usuario`);

--
-- Indices de la tabla `proveedores_has_producto`
--
ALTER TABLE `proveedores_has_producto`
  ADD KEY `fk_proveedores_has_producto_proveedores1_idx` (`proveedores_idproveedores`),
  ADD KEY `fk_proveedores_has_producto_producto1_idx` (`producto_idproducto`);

--
-- Indices de la tabla `reportes`
--
ALTER TABLE `reportes`
  ADD PRIMARY KEY (`idreportes`),
  ADD KEY `fk_reportes_chequeo1_idx` (`chequeo_idchequeo`),
  ADD KEY `fk_reportes_producto1_idx` (`producto_idproducto`),
  ADD KEY `fk_reportes_supervisor1_idx` (`supervisor_idsupervisor`),
  ADD KEY `fk_reportes_proveedores1_idx` (`proveedores_idproveedores`);

--
-- Indices de la tabla `supervisor`
--
ALTER TABLE `supervisor`
  ADD PRIMARY KEY (`idsupervisor`),
  ADD KEY `fk_supervisor_tipo_usuario1_idx` (`tipo_usuario_idtipo_usuario`);

--
-- Indices de la tabla `tamanos`
--
ALTER TABLE `tamanos`
  ADD PRIMARY KEY (`idtamanos`),
  ADD KEY `fk_tamanos_tipo_producto1_idx` (`tipo_producto_idtipo_producto`);

--
-- Indices de la tabla `telefonos`
--
ALTER TABLE `telefonos`
  ADD PRIMARY KEY (`idtelefonos`),
  ADD KEY `fk_telefonos_codigo_area1_idx` (`codigo_area_idcodigo_area`);

--
-- Indices de la tabla `tipo_producto`
--
ALTER TABLE `tipo_producto`
  ADD PRIMARY KEY (`idtipo_producto`),
  ADD KEY `fk_tipo_producto_clasificacion_producto1_idx` (`clasificacion_producto_idclasificacion_ropa`);

--
-- Indices de la tabla `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  ADD PRIMARY KEY (`idtipo_usuario`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD CONSTRAINT `fk_administrador_reportes1` FOREIGN KEY (`reportes_idreportes`) REFERENCES `reportes` (`idreportes`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_administrador_tipo_usuario1` FOREIGN KEY (`tipo_usuario_idtipo_usuario`) REFERENCES `tipo_usuario` (`idtipo_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `chequeo`
--
ALTER TABLE `chequeo`
  ADD CONSTRAINT `fk_chequeo_estatus1` FOREIGN KEY (`estatus_idestatus`) REFERENCES `estatus` (`idestatus`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_chequeo_producto1` FOREIGN KEY (`producto_idproducto`) REFERENCES `producto` (`idproducto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_chequeo_supervisor1` FOREIGN KEY (`supervisor_idsupervisor`) REFERENCES `supervisor` (`idsupervisor`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD CONSTRAINT `fk_clientes_tipo_usuario1` FOREIGN KEY (`tipo_usuario_idtipo_usuario`) REFERENCES `tipo_usuario` (`idtipo_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `clientes_has_producto`
--
ALTER TABLE `clientes_has_producto`
  ADD CONSTRAINT `fk_clientes_has_producto_clientes1` FOREIGN KEY (`clientes_idclientes`) REFERENCES `clientes` (`idclientes`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_clientes_has_producto_producto1` FOREIGN KEY (`producto_idproducto`) REFERENCES `producto` (`idproducto`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `login`
--
ALTER TABLE `login`
  ADD CONSTRAINT `fk_login_tipo_usuario` FOREIGN KEY (`tipo_usuario_idtipo_usuario`) REFERENCES `tipo_usuario` (`idtipo_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `persona`
--
ALTER TABLE `persona`
  ADD CONSTRAINT `fk_persona_estado_civil1` FOREIGN KEY (`estado_civil_idestado_civil`) REFERENCES `estado_civil` (`idestado_civil`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_persona_genero1` FOREIGN KEY (`genero_idgenero`) REFERENCES `genero` (`idgenero`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_persona_nacionalidad1` FOREIGN KEY (`nacionalidad_idnacionalidad`) REFERENCES `nacionalidad` (`idnacionalidad`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `persona_has_localidad`
--
ALTER TABLE `persona_has_localidad`
  ADD CONSTRAINT `fk_persona_has_localidad_localidad1` FOREIGN KEY (`localidad_idlocalidad`) REFERENCES `localidad` (`idlocalidad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_persona_has_localidad_persona1` FOREIGN KEY (`persona_idpersona`) REFERENCES `persona` (`idpersona`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `persona_has_telefonos`
--
ALTER TABLE `persona_has_telefonos`
  ADD CONSTRAINT `fk_persona_has_telefonos_persona1` FOREIGN KEY (`persona_idpersona`) REFERENCES `persona` (`idpersona`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_persona_has_telefonos_telefonos1` FOREIGN KEY (`telefonos_idtelefonos`) REFERENCES `telefonos` (`idtelefonos`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `fk_producto_tamanos1` FOREIGN KEY (`tamanos_idtamanos`) REFERENCES `tamanos` (`idtamanos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_producto_tipo_producto1` FOREIGN KEY (`tipo_producto_idtipo_producto`) REFERENCES `tipo_producto` (`idtipo_producto`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD CONSTRAINT `fk_proveedores_tipo_usuario1` FOREIGN KEY (`tipo_usuario_idtipo_usuario`) REFERENCES `tipo_usuario` (`idtipo_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `proveedores_has_producto`
--
ALTER TABLE `proveedores_has_producto`
  ADD CONSTRAINT `fk_proveedores_has_producto_producto1` FOREIGN KEY (`producto_idproducto`) REFERENCES `producto` (`idproducto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_proveedores_has_producto_proveedores1` FOREIGN KEY (`proveedores_idproveedores`) REFERENCES `proveedores` (`idproveedores`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `reportes`
--
ALTER TABLE `reportes`
  ADD CONSTRAINT `fk_reportes_chequeo1` FOREIGN KEY (`chequeo_idchequeo`) REFERENCES `chequeo` (`idchequeo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_reportes_producto1` FOREIGN KEY (`producto_idproducto`) REFERENCES `producto` (`idproducto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_reportes_proveedores1` FOREIGN KEY (`proveedores_idproveedores`) REFERENCES `proveedores` (`idproveedores`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_reportes_supervisor1` FOREIGN KEY (`supervisor_idsupervisor`) REFERENCES `supervisor` (`idsupervisor`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `supervisor`
--
ALTER TABLE `supervisor`
  ADD CONSTRAINT `fk_supervisor_tipo_usuario1` FOREIGN KEY (`tipo_usuario_idtipo_usuario`) REFERENCES `tipo_usuario` (`idtipo_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tamanos`
--
ALTER TABLE `tamanos`
  ADD CONSTRAINT `fk_tamanos_tipo_producto1` FOREIGN KEY (`tipo_producto_idtipo_producto`) REFERENCES `tipo_producto` (`idtipo_producto`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `telefonos`
--
ALTER TABLE `telefonos`
  ADD CONSTRAINT `fk_telefonos_codigo_area1` FOREIGN KEY (`codigo_area_idcodigo_area`) REFERENCES `codigo_area` (`idcodigo_area`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tipo_producto`
--
ALTER TABLE `tipo_producto`
  ADD CONSTRAINT `fk_tipo_producto_clasificacion_producto1` FOREIGN KEY (`clasificacion_producto_idclasificacion_ropa`) REFERENCES `clasificacion_producto` (`idclasificacion_ropa`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
